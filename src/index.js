import React from 'react';
import ReactDOM from 'react-dom';
import { RickAndMorty } from './RickAndMorty';

import './index.css';
import 'animate.css';


ReactDOM.render(
    <RickAndMorty />,
  document.getElementById('root')
);
