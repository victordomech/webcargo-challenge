import React, {useState} from 'react';
import { Pagination } from './components/Pagination';
import { CharactersGrid } from './components/CharactersGrid';

export const RickAndMorty = () => {

    const [page, setPage] = useState(1);

    return (
        <>
            <h2>Rick y Morty App</h2>

            <hr />

            <div>
                <CharactersGrid
                    key   = {page}
                    page  = {page}
                />
                <Pagination page ={ page } setPage ={ setPage }/>
            </div>
        </>
    )
}
