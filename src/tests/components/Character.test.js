import React from 'react';
import { shallow } from "enzyme";
import { Character } from "../../components/Character";
import PropTypes from 'prop-types';

describe('Test of <Character />', () => {

    
    const name      = "Rick Sanchez";
    const img       = "https://rickandmortyapi.com/api/character/avatar/1.jpeg";
    const status    = "Alive";

    const wrapper   = shallow( <Character id={ 1 } name={ name } img={ img } status={ status } />);

    test('It should display the component correctly', () => {

        expect( wrapper ).toMatchSnapshot();
    })

    test('It should display the name', () => {
        
        const test_name = wrapper.find('.card_name');
        expect( test_name.text().trim()).toBe(name);
    })

    test('It should display the image', () => {
        
        const test_img = wrapper.find('.card_image');
        expect (test_img.prop('src')).toBe(img);
        expect (test_img.prop('alt')).toBe(name);
    })

    test('It should display the status', () => {
        
        const test_status = wrapper.find('.card_status');
        expect( test_status.text().trim()).toBe(status);
    })

    test('It should have class correctly', () => {
        
        const div = wrapper.find('.card_status');
        const className = div.prop('className');

        expect ( className.includes(`card_status card_status--${status}`)).toBe(true);
    })
})

Character.propTypes = {

    id:     PropTypes.number.isRequired,
    name:   PropTypes.string.isRequired,
    img:    PropTypes.string.isRequired,
    status: PropTypes.string.isRequired 
}
