import {useState, useEffect} from 'react';
import { getCharacters } from '../helpers/getCharacters';

export const useFetchCharacters = ( page=1 ) => {

    const [state, setState] = useState({
        
        data: [],
        loading: true
    });

    useEffect( () =>{
       
        getCharacters( page )
        .then( characters => {
            setState({
                data: characters,
                loading: false,
            })

        });

    }, [ page ])

    return state; // {data:[], loading: true};
}
