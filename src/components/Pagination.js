
import React from 'react';
import propTypes from 'prop-types';

export const Pagination = ({page, setPage}) => {

    const handleFirstPage = (e) => {

        e.preventDefault();
        if(page !== 1){
            setPage(1);
        }
    }

    const handleBeforePage = (e) => {

        e.preventDefault();
        if(page !== 1){
            setPage(page - 1);
        }
    }

    const handleAfterPage = (e) => {

        e.preventDefault();
        if(page !== 34){
            setPage(page + 1);
        }
    }

    const handleLastPage = (e) => {

        e.preventDefault();
        if(page !== 34){
            setPage(34);
        }
    }

    return (
        <div className="pagination">
            <button  onClick =   { handleFirstPage }  >FIRST</button>
            <button  onClick =   { handleBeforePage } >{'<'}</button>
            <button  onClick =   { handleAfterPage }  >{'>'}</button>
            <button  onClick =   { handleLastPage }   >LAST</button>

        </div>
    )
}

Pagination.propTypes = {

    setPage: propTypes.func.isRequired,
    page: propTypes.number.isRequired
}
