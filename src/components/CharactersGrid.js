import React  from 'react';
import propTypes from 'prop-types';
import { useFetchCharacters } from '../hooks/useFetchCharacters';
import { Character } from './Character';


export const CharactersGrid = ({page}) => {

    const {data:characters,loading} = useFetchCharacters( page );

    return (
        <>
            <h3 className="animate__animated animate__fadeIn">Page {page}</h3>

            { loading ? 'Loading...' : null }
            { <div className="cards">
                {
                    characters.map( character =>(
                        <Character 
                            key     =   { character.id }
                            name    =   { character.name }
                            status  =   { character.status }
                            img     =   { character.img }
                        />
                    ))
                }
            </div> }
        </>
    )
}

CharactersGrid.propTypes = {

    page: propTypes.number.isRequired
}

