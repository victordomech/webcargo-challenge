import React from 'react';

export const Character = ({id, name, status, img}) => {

    return (
        <figure key={id} className="card animate__animated animate__fadeIn">
                <img className="card_image" src={img} alt={name} />
                <figcaption className="card_name">{name}</figcaption>
                <figcaption className={`card_status card_status--${status}`}>{status}</figcaption>
        </figure>
    )
}