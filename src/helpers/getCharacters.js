

export const getCharacters = async( page ) =>{

    const url       = `https://rickandmortyapi.com/api/character?page=${encodeURI( page )}`;
    const resp      = await fetch(url);
    const {results} = await resp.json();

    const characters = results.map ( character => {

        return {
            id: character.id,
            name: character.name,
            status: character.status,
            img: character.image,
        }
    })

    return characters;
}