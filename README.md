## Enunciado del ejercicio

A partir de esta api de Rick y Morty https://rickandmortyapi.com/api/character tienes que montar un listado de todos los personajes en react con un paginador (puedes pasarle al endpoint la variable page, por ejemplo: https://rickandmortyapi.com/api/character?page=1) y mostrar su imagen, nombre y si sigue vivo o no. Cada tarjeta de personaje tendrá que ser un componente de react llamado "character". Ten en cuenta el rendimiento y un mínimo de estilos. puedes utilizar cualquier librería de React.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start` or `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test` or `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build` or `npm build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

